const electron = require('electron')
// Module to control application life.
const {ipcMain}  = require('electron')
const EOL = require('os').EOL
const fs = require('fs')

const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')

//report ++
let CWD = process.cwd()

const rootDir = CWD

const jsreport = require('jsreport')({
  rootDirectory: rootDir
})
//report --

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600})

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })


  // handling action that was generated from renderer process
  ipcMain.on('render-start', async (event, args) => {
    console.log('initializing reporter..')
    try {
      // we defer jsreport initialization on first report render
      // to avoid slowing down the app at start time
      if (!jsreport._initialized) {
        await jsreport.init()
        console.log('jsreport started')
      }

      console.log('info', 'rendering report..')

      try {
        const resp = await jsreport.render({
          template: {
            content: fs.readFileSync(path.join(__dirname, './views/report.html')).toString(),
            engine: 'handlebars',
            recipe: 'chrome-pdf'
          },
          data: {
            rows: args
          }
        })

        console.log( 'report generated')

        fs.writeFileSync(path.join(CWD, 'report.pdf'), resp.content)

        const pdfWindow = new BrowserWindow({
              width: 1024,
              height: 800,
              webPreferences: {
                  plugins: true
              }
          })

          pdfWindow.loadURL(url.format({
          pathname: path.join(CWD, 'report.pdf'),
          protocol: 'file'
        }))

        event.sender.send('render-finish', {})
      } catch (e) {
        console.log('error', `error while generating or saving report: ${e.stack}`)
        event.sender.send('render-finish', { errorText: e.stack })
      }
    } catch (e) {
      console.log('error', `error while starting jsreport: ${e.stack}`)
      app.quit()
    }
  })

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
